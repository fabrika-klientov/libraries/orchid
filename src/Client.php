<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Orchid
 * @category  Orchid
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.16
 * @link      https://fabrika-klientov.ua
 */

namespace Orchid;


use Orchid\Core\HttpClient;
use Orchid\Exceptions\OrchidException;
use Orchid\Models\Contact;
use Orchid\Models\Owner;
use Orchid\Models\Source;

class Client
{
    /**
     * @var HttpClient $httpClient
     * */
    private $httpClient;

    /**
     * @param string $server
     * @param string $token
     * @return void
     * @throws OrchidException
     * */
    public function __construct(string $server = null, string $token = null)
    {
        $server = $server ?? getenv('ORCHID_SERVER');
        $token = $token ?? getenv('ORCHID_TOKEN');

        if (!$server) {
            throw new OrchidException('Server is required');
        }
        if (!$token) {
            throw new OrchidException('Token is required');
        }

        $this->httpClient = new HttpClient($server, $token);
    }

    /**
     * @return HttpClient
     * */
    public function getHttpClient()
    {
        return $this->httpClient;
    }

    /** instance of Source
     * @return Source
     * */
    public function sources()
    {
        return new Source($this->httpClient);
    }

    /** instance of Source
     * @return Owner
     * */
    public function owners()
    {
        return new Owner($this->httpClient);
    }

    /** instance of Contact
     * @return Contact
     * */
    public function contacts()
    {
        return new Contact($this->httpClient);
    }
}