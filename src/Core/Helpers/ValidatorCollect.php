<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Orchid
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.15
 * @link      https://fabrika-klientov.ua
 */

namespace Orchid\Core\Helpers;


use Orchid\Core\Collection;
use Orchid\Exceptions\OrchidException;

trait ValidatorCollect
{
    /**
     * @param Collection $collection
     * @param bool $isUpdate
     * @return bool
     * @throws OrchidException
     * */
    protected function validate(Collection $collection, bool $isUpdate = false)
    {
        if ($collection->isEmpty()) {
            throw new OrchidException('Collect is empty');
        }
        return $collection->every(function ($item) use ($isUpdate) {
            return $isUpdate ? isset($item->id) : empty($item->id);
        });
    }
}