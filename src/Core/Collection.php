<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Orchid
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.16
 * @link      https://fabrika-klientov.ua
 */

namespace Orchid\Core;


use Illuminate\Support\Collection as BaseCollection;
use Orchid\Exceptions\OrchidException;

class Collection extends BaseCollection
{
    /**
     * @param string $source
     * @param string $owner
     * @param string $user
     * @return array
     * @throws OrchidException
     * */
    public function store(string $source, string $owner, string $user = null)
    {
        /**
         * @var \Orchid\Models\Contact $first
         * */
        $first = $this->first();
        if (!method_exists($first, 'store')) {
            throw new OrchidException('In instance [' . get_class($first) . '] mass method [store] not supported');
        }

        return $first->store($source, $this, $owner, $user);
    }

    /**
     * @return array
     * @throws OrchidException
     * */
    public function update()
    {
        /**
         * @var \Orchid\Models\Contact $first
         * */
        $first = $this->first();
        if (!method_exists($first, 'update')) {
            throw new OrchidException('In instance [' . get_class($first) . '] mass method [update] not supported');
        }

        return $first->update($this);
    }

    /**
     * @return bool
     * @throws OrchidException
     * */
    public function destroy()
    {
        /**
         * @var \Orchid\Models\Contact $first
         * */
        $first = $this->first();
        if (!method_exists($first, 'destroy')) {
            throw new OrchidException('In instance [' . get_class($first) . '] mass method [destroy] not supported');
        }

        return $first->destroy($this);
    }

}