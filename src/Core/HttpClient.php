<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Orchid
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.15
 * @link      https://fabrika-klientov.ua
 */

namespace Orchid\Core;


use GuzzleHttp\Client;
use Orchid\Contract\BeHttpClient;
use Orchid\Exceptions\OrchidException;

class HttpClient implements BeHttpClient
{
    /**
     * @var Client $client
     * */
    private $client;
    private $token;

    /**
     * @param string $url
     * @param string $token
     * @return void
     * */
    public function __construct(string $url, string $token)
    {
        $this->token = $token;
        $this->client = new Client([
            'base_uri' => $url,
            'verify' => false, // delete
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $this->token,
            ],
        ]);
    }

    /** get method for API
     * @param string $path
     * @param array $query
     * @return array
     * @throws OrchidException
     * */
    public function get(string $path, array $query = [])
    {
        return $this->request('GET', $path, [
            'query' => $query,
        ]);
    }

    /** post method for API
     * @param string $path
     * @param array $data
     * @return array
     * @throws OrchidException
     * */
    public function post(string $path, array $data)
    {
        return $this->request('POST', $path, [
            'json' => $data,
        ]);
    }

    /** post method for API
     * @param string $path
     * @param array $data
     * @return array
     * @throws OrchidException
     * */
    public function patch(string $path, array $data)
    {
        return $this->request('PATCH', $path, [
            'json' => $data,
        ]);
    }

    /** post method for API
     * @param string $path
     * @return array
     * @throws OrchidException
     * */
    public function delete(string $path)
    {
        return $this->request('DELETE', $path, []);
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $options
     * @return array
     * @throws OrchidException
     * */
    protected function request(string $method, string $url, array $options)
    {
        try {
            $response = $this->client->request($method, $url, $options);

            $result = json_decode($response->getBody()->getContents(), true);
            if (is_null($result)) {
                throw new \Exception('Response is NULL');
            }

            if ($result['status'] === false) {
                throw new \Exception('Server returned status FALSE. Request operation ['
                    . ($result['operation'] ?? null) . ']. ERRORS:: ' . json_encode($result['data']));
            }

            return $result;
        } catch (\Exception $exception) {
            throw new OrchidException($exception->getMessage());
        }
    }

}