<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Orchid
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.15
 * @link      https://fabrika-klientov.ua
 */

namespace Orchid\Core;


use Orchid\Contract\BeBuilder;

/**
 * @property int $current_page
 * @property Collection $data
 * @property string $first_page_url
 * @property string $last_page_url
 * @property string $next_page_url
 * @property string $prev_page_url
 * @property string $path
 * @property int $from
 * @property int $last_page
 * @property int $per_page
 * @property int $to
 * @property int $total
 * */
class Paginate implements \JsonSerializable
{
    /**
     * @var HttpClient $httpClient
     * */
    protected $httpClient;

    /**
     * @var BeBuilder $builder
     * */
    protected $builder;

    /**
     * @var string $class
     * */
    protected $class;

    /**
     * @var array $data
     * */
    private $data = [];

    /**
     * @param HttpClient $httpClient
     * @param BeBuilder $builder
     * @param string $class
     * @param array $data
     * @return void
     * */
    public function __construct(HttpClient $httpClient, BeBuilder $builder, string $class, array $data = [])
    {
        $this->httpClient = $httpClient;
        $this->builder = $builder;
        $this->class = $class;
        $this->data = $data;
    }

    /** next page
     * @return static
     * */
    public function next()
    {
        $page = $this->current_page + 1 > $this->last_page ? $this->last_page : $this->current_page + 1;
        $this->builder->where('page', $page);

        return $this->getModel()->get();
    }

    /** prev page
     * @return static
     * */
    public function prev()
    {
        $page = $this->current_page - 1 < 1 ? 1 : $this->current_page - 1;
        $this->builder->where('page', $page);

        return $this->getModel()->get();
    }

    /** last page
     * @return static
     * */
    public function last()
    {
        $this->builder->where('page', $this->last_page);

        return $this->getModel()->get();
    }

    /** first page
     * @return static
     * */
    public function first()
    {
        $this->builder->where('page', 1);

        return $this->getModel()->get();
    }

    /** to page
     * @param int $page
     * @return static
     * */
    public function page(int $page)
    {
        $page = $page > $this->last_page ? $this->last_page : ($page < 1 ? 1 : $page);
        $this->builder->where('page', $page);

        return $this->getModel()->get();
    }

    /** count pages
     * @return int
     * */
    public function pages()
    {
        return $this->last_page;
    }

    /** total items
     * @return int
     * */
    public function total()
    {
        return $this->total;
    }

    /** current page
     * @return int
     * */
    public function currentPage()
    {
        return $this->current_page;
    }

    /** total items in page
     * @return int
     * */
    public function inPage()
    {
        return $this->per_page;
    }

    /** isset
     * @param string $name
     * @return bool
     * */
    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    /** getter
     * @param string $name
     * @return mixed
     * */
    public function __get($name)
    {
        return $this->data[$name] ?? null;
    }

    /** setter
     * @param string $name
     * @param mixed $value
     * @return void
     * */
    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    /** serializable
     * @return string
     * */
    public function __toString()
    {
        return (string)json_encode($this->data);
    }

    /** \JsonSerializable
     * @return array
     * */
    public function jsonSerialize()
    {
        return $this->data;
    }

    /** get tmp instance of model
     * @return mixed
     * */
    protected function getModel()
    {
        $model = new $this->class($this->httpClient);
        $model->setBuilder($this->builder);
        return $model;
    }
}