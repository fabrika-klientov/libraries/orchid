<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Orchid
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.16
 * @link      https://fabrika-klientov.ua
 */

namespace Orchid\Core\Build;

/**
 * @method $this source(string $source)
 * @method $this owner(string $owner)
 * @method $this search(string $find)
 * @method $this lazy(bool $value)
 * @method $this mask(bool $value)
 * @method $this page(int $page)
 * @method $this user(string $uuid) // only superuser
 * */
class ContactBuilder extends Builder
{

}