<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Orchid
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.15
 * @link      https://fabrika-klientov.ua
 */

namespace Orchid\Core\Build;


use Orchid\Contract\BeBuilder;

class Builder implements BeBuilder
{
    /**
     * @var array $data
     * */
    protected $data = [];

    /** where builder
     * @param string $key
     * @param mixed $value
     * @return $this
     * */
    public function where($key, $value)
    {
        $this->data[$key] = is_bool($value) ? ($value ? 1 : 0) : $value;
        return $this;
    }

    /** mixed property where
     * @param string $name
     * @param array $arguments
     * @return $this
     * */
    public function __call($name, $arguments)
    {
        $this->where($name, ...$arguments);
        return $this;
    }

    /**
     * @return array
     * */
    public function getResult()
    {
        return $this->data;
    }

}