<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Orchid
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.16
 * @link      https://fabrika-klientov.ua
 */

namespace Orchid\Models;

use Illuminate\Support\Collection;

/**
 * @property string $id
 * @property string $code
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 * */
final class Source extends Model
{
    protected function resourceLink(): string
    {
        return '/api/v1/sources';
    }

    /**
     * @param string $uuid
     * @return Collection
     * @throws \Orchid\Exceptions\OrchidException
     * */
    public function get(string $uuid = null)
    {
        $result = parent::get($uuid);
        return new Collection(array_map(function ($item) {
            return new static($this->httpClient, $item);
        }, $result['data'] ?? []));
    }
}