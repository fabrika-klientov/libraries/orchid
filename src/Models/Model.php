<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Orchid
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.15
 * @link      https://fabrika-klientov.ua
 */

namespace Orchid\Models;


use Orchid\Core\Build\Builder;
use Orchid\Core\HttpClient;
use Orchid\Exceptions\OrchidException;

abstract class Model implements \JsonSerializable
{
    /**
     * @var HttpClient $httpClient
     * */
    protected $httpClient;

    /**
     * @var \Orchid\Contract\BeBuilder $query
     * */
    protected $query;

    /**
     * @var array $data
     * */
    protected $data = [];

    /**
     * @param HttpClient $client
     * @param array $data
     * @return void
     * */
    public function __construct(HttpClient $client, array $data = [])
    {
        $this->httpClient = $client;
        $this->query = new Builder();
        $this->data = $data;
    }

    /**
     * @return HttpClient
     * */
    public function getHttpClient()
    {
        return $this->httpClient;
    }

    /**
     * @param \Orchid\Contract\BeBuilder $builder
     * @return $this
     * */
    public function setBuilder($builder)
    {
        $this->query = $builder;
        return $this;
    }

    /**
     * @param string $uuid
     * @return mixed
     * @throws \Orchid\Exceptions\OrchidException
     * */
    public function get(string $uuid = null)
    {
        return $this->httpClient->get($this->resourceLink() . (isset($uuid) ? "/{$uuid}" : ''), $this->query->getResult());
    }

    /**
     * @param string $uuid
     * @return static
     * @throws \Orchid\Exceptions\OrchidException
     * */
    public function find(string $uuid)
    {
        return $this->get($uuid);
    }

    /**
     * @return static
     * @throws \Orchid\Exceptions\OrchidException
     * */
    public function save()
    {
        if (isset($this->data['id'])) {
            $result = $this->httpClient->patch($this->resourceLink() . "/{$this->data['id']}", $this->data);
        } else {
            $result = $this->httpClient->post($this->resourceLink(), $this->data);
        }

        $this->data = $result['data'];
        return $this;
    }

    /**
     * @return bool
     * @throws \Orchid\Exceptions\OrchidException
     * */
    public function destroy()
    {
        if (empty($this->data['id'])) {
            throw new OrchidException('For destroy model [id] is required');
        }

        $result = $this->httpClient->delete($this->resourceLink() . "/{$this->data['id']}");

        return $result['status'];
    }

    /** isset
     * @param string $name
     * @return bool
     * */
    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    /** getter
     * @param string $name
     * @return mixed
     * */
    public function __get($name)
    {
        return $this->data[$name] ?? null;
    }

    /** setter
     * @param string $name
     * @param mixed $value
     * @return void
     * */
    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    /** serializable
     * @return string
     * */
    public function __toString()
    {
        return (string)json_encode($this->data);
    }

    /** \JsonSerializable
     * @return array
     * */
    public function jsonSerialize()
    {
        return $this->data;
    }

    /**
     * @return string
     * */
    protected abstract function resourceLink(): string;

}