<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Orchid
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.16
 * @link      https://fabrika-klientov.ua
 */

namespace Orchid\Models;


use Orchid\Contract\BePaginate;
use Orchid\Core\Build\ContactBuilder;
use Orchid\Core\Collection;
use Orchid\Core\Helpers\ValidatorCollect;
use Orchid\Core\HttpClient;
use Orchid\Core\Paginate;
use Orchid\Exceptions\OrchidException;

/**
 * @property string $id
 * @property string $name
 * @property string $user_id
 * @property string $source_id
 * @property string $created_at
 * @property string $updated_at
 * @property Source $source
 * @property string[] $phones
 * @property string[] $emails
 * @property string $remote_id
 * */
final class Contact extends Model implements BePaginate
{
    use ValidatorCollect;

    public function __construct(HttpClient $client, array $data = [])
    {
        parent::__construct($client, $data);
        $this->setBuilder(new ContactBuilder());
    }

    /** closure
     * @param \Closure $closure
     * @return $this
     * */
    public function where(\Closure $closure)
    {
        $closure($this->query);
        return $this;
    }

    /**
     * @param string $uuid
     * @param bool $isPaginate
     * @return Paginate|Contact|Collection|static
     * @throws \Orchid\Exceptions\OrchidException
     * */
    public function get(string $uuid = null, bool $isPaginate = true)
    {
        if (!$isPaginate) {
            $this->query->where('no_paginator', true);
        }

        $result = parent::get($uuid);

        if ($result['operation'] == 'index') {
            $result['data'] = new Collection(array_map(function ($item) {
                return self::getStatic($this->httpClient, $item);
            }, $result['data']));

            if ($isPaginate) {
                return new Paginate($this->httpClient, $this->query, self::class, $result);
            }

            return $result['data'];
        }

        return self::getStatic($this->httpClient, $result['data']);
    }

    /**
     * @param string $source
     * @param string $owner
     * @param string $user
     * @return static
     * @throws \Orchid\Exceptions\OrchidException
     * */
    public function save(string $source = null, string $owner = null, string $user = null)
    {
        if (isset($this->data['id'])) {
            $result = $this->httpClient->patch($this->resourceLink() . "/update", [
                'data' => [$this->data],
            ]);
        } elseif (isset($source, $owner)) {
            $result = $this->httpClient->post($this->resourceLink(), [
                'source' => $source,
                'owner' => $owner,
                'user' => $user,
                'data' => [$this->data],
            ]);
        } else {
            throw new OrchidException('[source], [owner] for store or [id] for update is required');
        }

        return self::getStatic($this->httpClient, head($result['data']));
    }

    /**
     * @param string $source
     * @param Collection $collection
     * @param string $owner
     * @param string $user
     * @return array ['resolved' => Collection, 'rejected' => Collection]
     * @throws OrchidException
     * */
    public function store(string $source, Collection $collection, string $owner, string $user = null)
    {
        if (!$this->validate($collection)) {
            throw new OrchidException('All items in collection should have id is null or undefined');
        }

        $result = $this->httpClient->post($this->resourceLink(), [
            'source' => $source,
            'owner' => $owner,
            'user' => $user,
            'data' => $collection->all(),
        ]);

        return [
            'resolved' => new Collection(array_map(function ($item) {
            return self::getStatic($this->httpClient, $item);
        }, $result['data'])),
            'rejected' => new Collection($result['rejected'] ?? []),
        ];
    }

    /**
     * @param Collection $collection
     * @return array ['resolved' => Collection, 'rejected' => Collection]
     * @throws OrchidException
     * */
    public function update(Collection $collection)
    {
        if (!$this->validate($collection, true)) {
            throw new OrchidException('All items in collection should have id is required');
        }

        $result = $this->httpClient->patch($this->resourceLink() . '/update', [
            'data' => $collection->all(),
        ]);

        return [
            'resolved' => new Collection(array_map(function ($item) {
                return self::getStatic($this->httpClient, $item);
            }, $result['data'])),
            'rejected' => new Collection($result['rejected'] ?? []),
        ];
    }

    /**
     * @param Collection $collection
     * @return bool
     * @throws OrchidException
     * */
    public function destroy(Collection $collection = null)
    {
        if (is_null($collection)) {
            return parent::destroy();
        }

        if (!$this->validate($collection, true)) {
            throw new OrchidException('All items in collection should have id is required');
        }

        $result = $this->httpClient->delete($this->resourceLink() . '/' . $collection->map(function ($item) {
                return $item->id;
            })->join(','));

        return $result['status'];
    }

    /** replace data in update
     * @param array $replace available ['phone', 'email']
     * @return $this
     * */
    public function replace(array $replace = null)
    {
        $this->data['replace'] = $replace;
        return $this;
    }

    /** remote source id
     * @param string $value
     * @return $this
     * */
    public function remoteID($value)
    {
        $this->data['remote_id'] = $value;
        return $this;
    }

    /** get new context
     * @param HttpClient $httpClient
     * @param array $item
     * @return static
     * */
    public static function getStatic(HttpClient $httpClient, array $item)
    {
        $contact = new static($httpClient, $item);
        $contact->source = new Source($httpClient, $item['source']);
        $contact->phones = array_map(function ($item) {
            return $item['phone'];
        }, $item['phones'] ?? []);
        $contact->emails = array_map(function ($item) {
            return $item['email'];
        }, $item['emails'] ?? []);
        return $contact;
    }

    protected function resourceLink(): string
    {
        return '/api/v1/query';
    }

}