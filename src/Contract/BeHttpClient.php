<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Orchid
 * @category  Contracts
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.15
 * @link      https://fabrika-klientov.ua
 */

namespace Orchid\Contract;


interface BeHttpClient
{
    /**
     * @param string $path
     * @param array $query
     * @return mixed
     * */
    public function get(string $path, array $query = []);

    /**
     * @param string $path
     * @param array $data
     * @return mixed
     * */
    public function post(string $path, array $data);

    /**
     * @param string $path
     * @param array $data
     * @return mixed
     * */
    public function patch(string $path, array $data);

    /**
     * @param string $path
     * @return mixed
     * */
    public function delete(string $path);
}