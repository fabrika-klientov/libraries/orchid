<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Orchid
 * @category  Contracts
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.15
 * @link      https://fabrika-klientov.ua
 */

namespace Orchid\Contract;


interface BeBuilder
{
    /** where builder
     * @param string $key
     * @param mixed $value
     * @return $this
     * */
    public function where($key, $value);

    /**
     * @return array
     * */
    public function getResult();
}