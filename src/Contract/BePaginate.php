<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Orchid
 * @category  Contracts
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.15
 * @link      https://fabrika-klientov.ua
 */

namespace Orchid\Contract;


use Orchid\Core\HttpClient;

interface BePaginate
{
    /** action
     * @param HttpClient $httpClient
     * @param array $item
     * @return static
     * */
    public static function getStatic(HttpClient $httpClient, array $item);
}