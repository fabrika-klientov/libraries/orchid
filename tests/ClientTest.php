<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Orchid
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 20.01.31
 * @link      https://fabrika-klientov.ua
 */

namespace Tests;

use Orchid\Client;
use Orchid\Core\HttpClient;
use Orchid\Exceptions\OrchidException;
use Orchid\Models\Contact;
use Orchid\Models\Owner;
use Orchid\Models\Source;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    /**
     * @var Client $client
     * */
    protected $client;

    public function test__construct()
    {
        $client = new Client('https://orchid.com', 'bearer_token');

        $this->assertInstanceOf(Client::class, $client);

        $this->expectException(OrchidException::class);

        new Client();
        new Client('https://orchid.com');
        new Client(null, 'bearer_token');
    }

    public function testGetHttpClient()
    {
        $this->assertInstanceOf(HttpClient::class, $this->client->getHttpClient());
    }

    public function testSources()
    {
        $this->assertInstanceOf(Source::class, $this->client->sources());
    }

    public function testOwners()
    {
        $this->assertInstanceOf(Owner::class, $this->client->owners());
    }

    public function testContacts()
    {
        $this->assertInstanceOf(Contact::class, $this->client->contacts());
    }

    protected function setUp(): void
    {
        $this->client = new Client('https://orchid.com', 'bearer_token');
    }

    protected function tearDown(): void
    {
        $this->client = null;
    }
}
