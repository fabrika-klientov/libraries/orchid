## Status

[![Latest Stable Version](https://poser.pugx.org/shadoll/orchid/v/stable)](https://packagist.org/packages/shadoll/orchid)
[![pipeline status](https://gitlab.com/fabrika-klientov/libraries/orchid/badges/master/pipeline.svg)](https://gitlab.com/fabrika-klientov/libraries/orchid/commits/master)
[![License](https://poser.pugx.org/shadoll/orchid/license)](https://packagist.org/packages/shadoll/orchid)

---

## Usage

`composer require shadoll/orchid`


## Что это

Клиент для подключения к сервису Хранилища Контактной Информации (далее Сервис). Дает простой 
функционал получения, обновления, добавления и удаления контактной информации.
Сервис [orchid](https://gitlab.com/fakl/boxes/orchid).

## Client

Для начала работы необходимо создать Клиент `\Orchid\Client`. 

```php
$client = new \Orchid\Client();
// или
$client = new \Orchid\Client('https://YOUR.SERVER', 'eyJ0eXAiOiJKV1Q....');

```
В первом случае Клиент попытается найти значения Сервера в переменной окружения `ORCHID_SERVER` и 
токен в `ORCHID_TOKEN`. (токен можете получить через авторизацию на сервисе или через дашбоард сервиса)

В случае если переменные в конечном счете в конструкторе не будут определены - будет выброшено исключение
`\Orchid\Exceptions\OrchidException`. Исключения могут быть выброшены при любом запросе (выборки, изменения, ...) 
Обрабатывайте их.

Все данные которые могут иметь более 1 модели будут помещены в коллекции `\Orchid\Core\Collection`.

Из клиента можем получить инстансы моделей `Orchid\Models\Contact` и `Orchid\Models\Source`:

```php
// client
/**
 * @var \Orchid\Models\Source $source
 */
$source = $client->sources();

/**
 * @var \Orchid\Models\Contact $contact
 */
$contact = $client->contacts();

```

## Работа с источниками (Source)

Получить источники может любой пользователь имеющий токен нужный `scopes` (local), добавлять, изменять
и удалять исочники может только пользователь имеющий `scopes` -> `superuser`.

```php
// client
/**
 * @var \Orchid\Models\Source $source
 */
$source = $client->sources();

/** получить все источники
 * @var \Orchid\Core\Collection $collect
 */
$collect = $source->get();

/** получить источник по его UUID
 * @var \Orchid\Models\Source $model
 */
$model = $source->find('__UUID__');

// дальше только для Superuser

// Создание источника
$source1 = new \Orchid\Models\Source($client->getHttpClient(), [
    'name' => 'Jarvis source',
    'code' => 'jarvis_code',
]);

$source2 = new \Orchid\Models\Source($client->getHttpClient());
$source2->name = 'Google source';
$source2->code = 'google_code';

$source1->save();
$source2->save();

// Обновление источника
$model = $source->find('__UUID__');

$model->name = 'Updated Jarvis Name';

$model->save();

// Удаление источника
$model = $source->find('__UUID__');

$model->destroy();

```


## Работа с Контактами (Contact)

Обычный пользователь с `scopes` (local) может работать только со своими контактами,
 пользователь имеющий `scopes` -> `superuser` может работать с любыми контактами. (этот пользователь не 
 рекомендуется для клиентских решений).
 
 
**Выборка:**

```php
// client
/**
 * @var \Orchid\Models\Contact $contact
 */
$contact = $client->contacts();

/** получить все контакты (получите обьект пагинации)
 * @var \Orchid\Core\Paginate $paginate
 */
$paginate = $contact->get();
// или с фильтрацией
$paginate = $contact->where(function (\Orchid\Core\Build\ContactBuilder $builder) {
    $builder
        ->source('6a7e5826-f202-4678-99fb-e69bae8a1d5d')
        ->search('80999999999')
        ->mask(true);
})->get();

/** получить контакт по его UUID
 * @var \Orchid\Models\Contact $model
 */
$model = $contact->find('__UUID__');

```
---

Так как контактов у пользователя может быть достаточно много - выборка осуществляется по 100 штук.
Возвращается обьект `\Orchid\Core\Paginate`. 
Работа с `\Orchid\Core\Paginate`:
```php

/** получить все контакты (получите обьект пагинации)
 * @var \Orchid\Core\Paginate $paginate
 */
$paginate = $contact->get();

/** получить контакты текущей страницы (пагинации)
 * @var \Orchid\Core\Collection<\Orchid\Models\Contact> $collection
 */
$collection = $paginate->data;

/** получить все контакты (получите обьект пагинации) для следующей страницы
 * @var \Orchid\Core\Paginate $paginateNext
 */
$paginateNext = $paginate->next();

/** получить все контакты (получите обьект пагинации) для предыдущей страницы
 * @var \Orchid\Core\Paginate $paginatePrev
 */
$paginatePrev = $paginate->prev();

/** получить все контакты (получите обьект пагинации) для конкретной страницы
 * @var \Orchid\Core\Paginate $paginateFive
 */
$paginateFive = $paginate->page(5);

// еще доступны last(), first(), pages(), total(), currentPage(), inPage()
// для работы с пагинацией.

---

```

**Создание контакта:**

```php
// client
/**
 * @var \Orchid\Models\Contact $contact
 */
$contactInstance = $client->contacts();

$source = $client->sources()->get()->first();

$contact = new \Orchid\Models\Contact($client->getHttpClient(), [
    'name' => 'Jarvis',
    'phone' => ['0999999999', '0988888888'],
    'email' => ['jarvis@google.com', 'jarvis.2@google.com'],
]);

$contact->save($source->id);

// Массовое создание

$collect = new \Orchid\Core\Collection([
    (new \Orchid\Models\Contact($client->getHttpClient(), [
        'name' => 'Jarvis',
        'phone' => ['0999999999', '0988888888'],
        'email' => ['jarvis@google.com', 'jarvis.2@google.com'],
    ]))->remoteID('amo_id'),
    new \Orchid\Models\Contact($client->getHttpClient(), [
        'name' => 'Jarvis 2',
        'phone' => ['0999999998', '0988888889'],
        'email' => ['jarvis_2@google.com', 'jarvis.3@google.com'],
    ]),
]);

// 1 способ - через инстанс . (обязательно UUID или code источника)
$contactInstance->store($source->id, $collect);

// 2 способ - через коллекцию . (обязательно UUID или code источника)
$collect->store($source->code);

// superuser может создать контакты для любого пользователя

$contact->save($source->id, '__UUID_USER__');

$contactInstance->store($source->id, $collect, '__UUID_USER__');

$collect->store($source->code, '__UUID_USER__');

```
Возвращается массив с двумя коллекциями `['resolved' => Collection, 'rejected' => Collection]`

---

**Обновление контакта:**

```php
// client
/**
 * @var \Orchid\Models\Contact $contact
 */
$contactInstance = $client->contacts();

$contact = $contactInstance->find('__UUID__');
$contact->phone = ['0977777777'];
$contact->replace(['phone']); // это отвяжет все предыдущие телефоны и привяжет только новый
$contact->save();

// Массовое обновление

$collect = $contactInstance->where(/* filter Closure */)->get();

$collect->each(function($item) {
    if (in_array('0999999999', $item->phone)) {
        $item->replace(['phone', 'email']);
    }
    $item->phone = ['0988888777'];
    // ...
});

// 1 способ - через инстанс 
$contactInstance->update($collect);

// 2 способ - через коллекцию 
$collect->update();

```
Возвращается массив с двумя коллекциями `['resolved' => Collection, 'rejected' => Collection]`

---

**Удаление контакта:**

```php
// client
/**
 * @var \Orchid\Models\Contact $contact
 */
$contactInstance = $client->contacts();

$contact = $contactInstance->find('__UUID__');
$contact->destroy();

// Массовое удаление

$collect = $contactInstance->where(/* filter Closure */)->get();

$filteredCollect = $collect->filter(function($item) {
    return in_array('0999999999', $item->phone);
});

// 1 способ - через инстанс 
$contactInstance->destroy($filteredCollect);

// 2 способ - через коллекцию 
$filteredCollect->destroy();

```

---